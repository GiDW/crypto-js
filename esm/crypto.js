/* eslint-env es6 */

const padding =
  "\u0080\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000";
const k = [
  0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
  0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
  0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
  0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
  0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
  0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
  0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
  0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
  0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
  0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
  0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
];

export function createHash() {
  const md = {
    algorithm: "sha256",
    blockLength: 64,
    digestLength: 32,
    messageLength: 0,
    fullMessageLength: [0, 0],
    messageLengthSize: 8,
  };

  const state = {
    h0: 0x6a09e667,
    h1: 0xbb67ae85,
    h2: 0x3c6ef372,
    h3: 0xa54ff53a,
    h4: 0x510e527f,
    h5: 0x9b05688c,
    h6: 0x1f83d9ab,
    h7: 0x5be0cd19,
  };

  let input = "";
  let read = 0;

  const w = new Array(64);

  md.update = update;
  md.digest = digest;

  return md;

  function update(msg) {
    const _msg = encodeUTF8(msg);
    const length = _msg.length;
    md.messageLength += length;
    const arr = [(length / 0x100000000) >>> 0, length >>> 0];

    for (let i = md.fullMessageLength.length - 1; i >= 0; --i) {
      md.fullMessageLength[i] += arr[1];
      arr[1] = arr[0] + ((md.fullMessageLength[i] / 0x100000000) >>> 0);
      md.fullMessageLength[i] = md.fullMessageLength[i] >>> 0;
      arr[0] = (arr[1] / 0x100000000) >>> 0;
    }

    input += _msg;

    read = _update(state, w, input, read);

    return md;
  }

  function digest() {
    let finalBlock = input.slice(read);
    const remaining =
      md.fullMessageLength[md.fullMessageLength.length - 1] +
      md.messageLengthSize;
    const overflow = remaining & (md.blockLength - 1);
    const endIdx = md.blockLength - overflow;
    if (endIdx > 0) finalBlock += padding.substring(0, endIdx);

    let bits = md.fullMessageLength[0] * 8;
    for (let i = 0; i < md.fullMessageLength.length - 1; i++) {
      const next = md.fullMessageLength[i + 1] * 8;
      const carry = (next / 0x100000000) >>> 0;
      bits += carry;
      finalBlock += toInt32(bits >>> 0);
      bits = next >>> 0;
    }
    finalBlock += toInt32(bits);

    const s2 = {
      h0: state.h0,
      h1: state.h1,
      h2: state.h2,
      h3: state.h3,
      h4: state.h4,
      h5: state.h5,
      h6: state.h6,
      h7: state.h7,
    };

    _update(s2, w, finalBlock, 0);

    finalBlock =
      toInt32(s2.h0) +
      toInt32(s2.h1) +
      toInt32(s2.h2) +
      toInt32(s2.h3) +
      toInt32(s2.h4) +
      toInt32(s2.h5) +
      toInt32(s2.h6) +
      toInt32(s2.h7);

    let hex = "";

    const length = finalBlock.length;
    for (let i = 0; i < length; i++) {
      const code = finalBlock.charCodeAt(i);
      if (code < 16) hex += "0";
      hex += code.toString(16);
    }

    return hex;
  }
}

export function encodeUTF8(value) {
  return unescape(encodeURIComponent(value));
}

export function decodeUTF8(value) {
  return decodeURIComponent(escape(value));
}

function toInt32(value) {
  return (
    String.fromCharCode((value >> 24) & 0xff) +
    String.fromCharCode((value >> 16) & 0xff) +
    String.fromCharCode((value >> 8) & 0xff) +
    String.fromCharCode(value & 0xff)
  );
}

function _update(s, w, input, read) {
  let i, t1, t2, s0, s1, ch, maj, a, b, c, d, e, f, g, h;
  let len = input.length - read;
  while (len >= 64) {
    // the w array will be populated with sixteen 32-bit big-endian words
    // and then extended into 64 32-bit words according to SHA-256
    for (i = 0; i < 16; ++i) {
      w[i] =
        (input.charCodeAt(read++) << 24) ^
        (input.charCodeAt(read++) << 16) ^
        (input.charCodeAt(read++) << 8) ^
        input.charCodeAt(read++);
    }
    for (; i < 64; ++i) {
      // XOR word 2 words ago rot right 17, rot right 19, shft right 10
      t1 = w[i - 2];
      t1 =
        ((t1 >>> 17) | (t1 << 15)) ^ ((t1 >>> 19) | (t1 << 13)) ^ (t1 >>> 10);
      // XOR word 15 words ago rot right 7, rot right 18, shft right 3
      t2 = w[i - 15];
      t2 = ((t2 >>> 7) | (t2 << 25)) ^ ((t2 >>> 18) | (t2 << 14)) ^ (t2 >>> 3);
      // sum(t1, word 7 ago, t2, word 16 ago) modulo 2^32
      w[i] = (t1 + w[i - 7] + t2 + w[i - 16]) | 0;
    }

    // initialize hash value for this chunk
    a = s.h0;
    b = s.h1;
    c = s.h2;
    d = s.h3;
    e = s.h4;
    f = s.h5;
    g = s.h6;
    h = s.h7;

    // round function
    for (i = 0; i < 64; ++i) {
      // Sum1(e)
      s1 =
        ((e >>> 6) | (e << 26)) ^
        ((e >>> 11) | (e << 21)) ^
        ((e >>> 25) | (e << 7));
      // Ch(e, f, g) (optimized the same way as SHA-1)
      ch = g ^ (e & (f ^ g));
      // Sum0(a)
      s0 =
        ((a >>> 2) | (a << 30)) ^
        ((a >>> 13) | (a << 19)) ^
        ((a >>> 22) | (a << 10));
      // Maj(a, b, c) (optimized the same way as SHA-1)
      maj = (a & b) | (c & (a ^ b));

      // main algorithm
      t1 = h + s1 + ch + k[i] + w[i];
      t2 = s0 + maj;
      h = g;
      g = f;
      f = e;
      // `>>> 0` necessary to avoid iOS/Safari 10 optimization bug
      // can't truncate with `| 0`
      e = (d + t1) >>> 0;
      d = c;
      c = b;
      b = a;
      // `>>> 0` necessary to avoid iOS/Safari 10 optimization bug
      // can't truncate with `| 0`
      a = (t1 + t2) >>> 0;
    }

    // update hash state
    s.h0 = (s.h0 + a) | 0;
    s.h1 = (s.h1 + b) | 0;
    s.h2 = (s.h2 + c) | 0;
    s.h3 = (s.h3 + d) | 0;
    s.h4 = (s.h4 + e) | 0;
    s.h5 = (s.h5 + f) | 0;
    s.h6 = (s.h6 + g) | 0;
    s.h7 = (s.h7 + h) | 0;
    len -= 64;
  }
  return read;
}
