export declare function createHash(): GMesageDigest;
export declare class GMesageDigest {
  algorithm: string;
  blockLength: number;
  digestLength: number;
  messageLength: number;
  fullMessageLength: number[];
  messageLengthSize: number;
  update: (msg: string) => GMesageDigest;
  digest: () => string;
}
