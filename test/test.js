/* eslint-env node, mocha */

"use strict";

import assert from "assert";

import crypto from "crypto";
import * as gidwCryptoEsm from "../esm/crypto.js";
import * as gidwCryptoEsmMin from "../esm/crypto.min.js";
import gidwCryptoUmd from "../umd/crypto.js";
import gidwCryptoUmdMin from "../umd/crypto.min.js";

testSha256(assert, crypto, gidwCryptoEsm, "Crypto (ESM)");

testSha256(assert, crypto, gidwCryptoEsmMin, "Crypto (ESM) min");

testSha256(assert, crypto, gidwCryptoUmd, "Crypto (UMD)");

testSha256(assert, crypto, gidwCryptoUmdMin, "Crypto (UMD) min");

function testSha256(assert, nodeCrypto, gidwCrypto, testName) {
  describe(testName, function () {
    it("Should calculate a correct hash for a simple string", function () {
      const input = "dfkje-vrklj459?,.23+-ljkk!@#?fl";
      assert.strictEqual(
        nodeCrypto.createHash("sha256").update(input).digest("hex"),
        gidwCrypto.createHash().update(input).digest()
      );
    });
    it("Should calculate a correct hash for a complex string", function () {
      const input = "யாமறிந்த மொழிகளிலே தமிழ்மொழி போல் இனிதாவது";
      assert.strictEqual(
        nodeCrypto.createHash("sha256").update(input).digest("hex"),
        gidwCrypto.createHash().update(input).digest()
      );
    });
    it("Should calculate a correct hash for a complex string", function () {
      const input = "யாமறிந்த 😁😂😃😄😍😖😡😱😻 rklj459?,.23+-";
      assert.strictEqual(
        nodeCrypto.createHash("sha256").update(input).digest("hex"),
        gidwCrypto.createHash().update(input).digest()
      );
    });
    it("Should encode and deocde UTF-8 with same result", function () {
      const input = "யாமறிந்த 😁😂😃😄😍😖😡😱😻 rklj459?,.23+-";
      assert.strictEqual(
        gidwCrypto.decodeUTF8(gidwCrypto.encodeUTF8(input)),
        input
      );
    });
  });
}
