/* eslint-env node */

"use strict";

var fs = require("fs");
var crypto = require("crypto");

var forge = require("node-forge");
var cryptoJs = require("crypto-js");

var gidwCrypto = require("../..");

var RANDOM_DICT =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" +
  "0123456789" +
  " ?>,.][['!@#$%^&*(){}+-=_~" +
  "😁😂😃😄😍😖😡😱😻🌄🍑🏩᛫ᛇᚻᚠᚱᚩᚠᚢᚱᛦᛚᚳஎங்கும்இனிதாவது";
RANDOM_DICT = [...RANDOM_DICT];
var RANDOM_LENGTH = RANDOM_DICT.length;

testStrings([
  "absdkjlkdsfioejrlkjfdksjfklfsfsdf561d56f4546?>,.][['23",
  "ᚠᛇᚻ᛫ᛒᛦᚦ᛫ᚠᚱᚩᚠᚢᚱ᛫ᚠᛁᚱᚪ᛫ᚷᛖᚻᚹᛦᛚᚳᚢᛗ",
  "He wonede at Ernleȝe at æðelen are chirechen,",
  "யாமறிந்த மொழிகளிலே தமிழ்மொழி போல் இனிதாவது எங்கும் காணோம்",
  "😁😂😃😄😍😖😡😱😻",
  "🌄🍑🏩",
  generateRandom(10000),
]);

generateTestFile("test_file.txt", 10000, (error) => {
  if (error) {
    console.warn("Error writing test file", error);
  } else {
    testFile("test_file.txt");
  }
});

/**
 * @param {string[]} strings
 */
function testStrings(strings) {
  strings.forEach((val, i) => {
    console.info(
      `Hash ${i}\n${val}\n\n` +
        `SHA256 - node\n${sha256Node(val)}\n` +
        `SHA256 - forge\n${sha256Forge(val)}\n` +
        `SHA256 - crypto-js\n${sha256CryptoJs(val)}\n` +
        `SHA256 - gidw\n${sha256Gidw(val)}\n` +
        `SHA256 - UTF-8 - node\n${sha256Node(val, "utf8")}\n` +
        `SHA256 - UTF-8 - forge\n${sha256Forge(val, "utf8")}\n` +
        `SHA256 - UTF-8 - crypto-js\n${sha256CryptoJs(val, "utf8")}\n` +
        `SHA256 - UTF-8 - gidw\n${sha256Gidw(val, "utf8")}\n`
    );
  });
}

/**
 * @param {string} filePath
 * @param {number} size
 */
function generateTestFile(filePath, size, callback) {
  var cbCalled = false;
  var ws = fs.createWriteStream(filePath);
  ws.on("error", (error) => {
    if (!cbCalled && callback) {
      cbCalled = true;
      callback(error);
    }
  });
  ws.on("close", () => {
    if (!cbCalled && callback) {
      cbCalled = true;
      callback(null);
    }
  });
  ws.write(generateRandom(size));
  ws.close();
}

/**
 * @param {string} filePath
 */
function testFile(filePath) {
  var h1 = crypto.createHash("sha256");
  var h2 = forge.sha256.create();
  var h3 = gidwCrypto.createHash();

  var h4 = crypto.createHash("sha256");
  var h5 = forge.sha256.create();
  var h6 = gidwCrypto.createHash();

  var rs = fs.createReadStream(filePath);

  rs.on("error", (error) => console.log("READ ERROR", error));

  rs.on("data", (data) => {
    console.info("FILE DATA");

    h1.update(data);
    h2.update(data);
    h3.update(data);

    h4.update(data.toString(), "utf8");
    h5.update(data.toString(), "utf8");
    h6.update(data.toString(), "utf8");
  });

  rs.on("close", () => {
    console.info(
      "FILE CLOSE\n\n" +
        `SHA256 - Node\n${h1.digest("hex")}\n` +
        `SHA256 - Forge\n${h2.digest().toHex()}\n` +
        `SHA256 - basicCrypto\n${h3.digest()}\n` +
        `SHA256 - toString - Node\n${h4.digest("hex")}\n` +
        `SHA256 - toString - Forge\n${h5.digest().toHex()}\n` +
        `SHA256 - toString - basicCrypto\n${h6.digest()}\n`
    );
  });
}

function sha256Node(data, encoding) {
  return crypto.createHash("sha256").update(data, encoding).digest("hex");
}

function sha256Forge(data, encoding) {
  return forge.sha256.create().update(data, encoding).digest().toHex();
}

function sha256CryptoJs(data, encoding) {
  return cryptoJs.SHA256(data);
}

function sha256Gidw(data, encoding) {
  return gidwCrypto.createHash().update(data).digest();
}

function generateRandom(size) {
  var result = "";
  for (let i = 0; i < size; i++) {
    const idx = Math.floor(Math.random() * RANDOM_LENGTH);
    result += RANDOM_DICT[idx];
  }
  return result;
}
