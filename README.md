# crypto-js

Basic crypto functions

## Usage

```javascript
import { createHash } from "@gidw/crypto-js";

const sha256 = createHash().update("input data").digest();
```

```javascript
var gidwCrypto = require("@gidw/crypto-js");

var sha256 = gidwCrypto.createHash().update("input data").digest();
```
